<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use \App\User;
use \App\Mail\Activation;
use \App\Jobs\SendActivationEmail;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'verify']);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function store()
    {
        $this->validate(request(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:5'
        ]);


        $user = User::where('email', request('email'))->first();
        if ($user == null)
        {
            //Create User
            $user = User::create([
                        'email' => request('email'),
                        'password' => Hash::make(request('password')),
                        'email_token' => base64_encode(request('email'))
            ]);

            //Send Email
            dispatch(new SendActivationEmail($user));

            //Login User
            auth()->login($user);

            //Display a Message
            session()->flash('message', "New user created. A verification email will be sent to {$user->email} shortly.");

            //Redirect to Home
            return redirect()->home();
        } else
        {
            //Authenticate User
            $credentials = request()->only('email', 'password');
            if (auth()->attempt($credentials))
                return redirect()->home();
            else
            {
                session()->flash('error', 'Invalid Credentials!');
                return back();
            }
        }
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->home();
    }

    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        if (!empty($user))
        {
            $user->status = User::ACTIVE;
            $user->email_token = NULL;
            if ($user->save())
            {
                // session()->flash('message', "Congratulations! Your account is now active.");
                session()->flash('gMessage', "Congratulations! Your account is now active.");
            }
            else
                session()->flash('gError', "Could not activate your account");
        }
        else
            session()->flash('gError', "The token is not valid");
        return auth()->check() ? redirect()->home() : redirect('login');
    }

}
