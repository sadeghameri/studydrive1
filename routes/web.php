<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::redirect('/home', '/', 301);


Route::get('login', 'AuthController@login')->name('login');
Route::post('login', 'AuthController@store');
Route::post('logout', 'AuthController@logout')->name('logout');
Route::get('/verifyemail/{token}', 'AuthController@verify');
