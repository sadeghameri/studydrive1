@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Home') }}</div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-md-1">
                	       {{ __('Id') }}
                        </dt>
                        <dd class="col-md-11">
                           {{ $user->id }}
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-md-1">
                           {{ __('Email') }}
                        </dt>
                        <dd class="col-md-11">
                           {{ $user->email }}
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-md-1">
                           {{ __('Status') }}
                        </dt>
                        <dd class="col-md-11">
                           {{ $user->status() }}
                        </dd>
                    </dl>
                    <a class="btn btn-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                    </a>
                </div>
                @if(!empty(session('message')))
                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12">
                            <div class="alert alert-success" role="alert">{{ session('message')}}</div>    
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
