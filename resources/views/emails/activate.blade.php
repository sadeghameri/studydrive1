<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
	<h1>{{ __('Welcome to')}} {{ config('app.name', 'Laravel') }}</h1>
	<h4>
		{{ __('Click the following link to verify your email') }}<br/>
		{{ url("/verifyemail/$email_token") }}
	</h4>
</body>
</html>
